let userNumber = +prompt("Your number:");

while (userNumber === 0 || Number(isNaN(userNumber) || !Number.isInteger(userNumber))) {
    userNumber = +prompt("Repeat number:");
}

const result = [];

for (let i = 1; i <= userNumber; i++) {
    if (i % 5 === 0) {
        result.push(i);
    }
    else if (userNumber < 5) {
        console.log("Sorry. no numbers");
        break;
    }
}
console.log(result);
