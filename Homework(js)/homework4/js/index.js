function getFirstUserNumber() {
    let userFirstNumber = +prompt("Напиши первое число:");
    while (userFirstNumber === 0 || Number(isNaN(userFirstNumber))) {
        userFirstNumber = +prompt("Повтори число:", userFirstNumber);
    }
    return userFirstNumber;
}
function getSecondUserNumber() {
    let userSecondNumber = +prompt("Напиши второепш число:");
    while (userSecondNumber === 0 || Number(isNaN(userSecondNumber))) {
        userSecondNumber = +prompt("Повтори число:", userSecondNumber);
    }
    return userSecondNumber;
}
function getMathSymbol () {
    let userMathSymbol = prompt("Выбери любой математический символ +,-,*,/:");
    while (!/^[\-+*(/)]/.test(userMathSymbol)) {
        userMathSymbol = prompt("Повтори еще раз:", userMathSymbol);
    }
    return userMathSymbol;
}
function calcNumbers() {
    const mathSymbol = getMathSymbol ();
    switch (mathSymbol) {
        case '+': {
            return  getFirstUserNumber() + getSecondUserNumber();
        }
        case '-': {
            return  getFirstUserNumber() - getSecondUserNumber();
        }
        case '*': {
            return  getFirstUserNumber() * getSecondUserNumber();
        }
        case '/': {
            return  getFirstUserNumber() / getSecondUserNumber();
        }
    }
}

console.log(calcNumbers());
